var leadSystemEndpoint = "https://bbl-leads.techmarketing.co.il/api/leads";

window.bbl_leads = {
    send: function (data) {
        return jQuery.ajax({
            url: leadSystemEndpoint,
            method: 'POST',
            data: data
        });
    },
    sanitizeParams: function (xpm_data) {
        var leadSystemData = Object.assign({}, xpm_data);

        leadSystemData.campaign_id = leadSystemData.campaign_code;
        leadSystemData.crm_file_no = leadSystemData.fileNo;
        delete leadSystemData.campaign_code;
        delete leadSystemData.fileNo;

        for (var key in leadSystemData) {
            if (leadSystemData.hasOwnProperty(key) && ((typeof leadSystemData[key]) !== 'function')) {
                leadSystemData[key] = leadSystemData[key].toString();
            }
        }

        return leadSystemData;
    }
};