var $top_wrapper;
var xpm_data = {};
xpm_endpoint = "https://secure.xpm.co.il/BBL/SendLeadsWSnew/postData.aspx";
var env_production = false;

localizeParsley();
parsleyCustomValidations();


$(document).ready(function () {

    $top_wrapper = $("#top_wrapper");

    addIphoneClasses();

    if (!c2c_open()) {
        $("#c2c").hide()
    }

    if (typeof (BI) !== 'undefined') {
        // check if the Object BI exists. If it does then we are in production (exists on bezeqint server through their code)
        env_production = true;
    }

    // start setting values for old leads system
    xpm_data.ref = getQueryStringParam('ref');
    xpm_data.remarks = window.location.href; // add the current address
    xpm_data.campaign_code = 5278; // campaign code
    xpm_data.fileNo = $("#fileNo").val();

    // send all utm_data
    xpm_data.utm_source = getQueryStringParam('utm_source');
    xpm_data.utm_medium = getQueryStringParam('utm_medium');
    xpm_data.utm_campaign = getQueryStringParam('utm_campaign');
    xpm_data.utm_content = getQueryStringParam('utm_content');
    xpm_data.utm_term = getQueryStringParam('utm_term');

    // Handle radios
    $(".radio-input").on('change', function () {

        $(".radio-errors").removeClass('has-error');
        $(".styled-label").removeClass('checked');
        $(this).parent('label').addClass('checked');
    });

    $(".styled-label").on('keypress', function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(this).click();
        }
    });


    // Bezeqint form init. adds form action and phone prefix options...
    if (env_production) {
        BIformInit();
    }
    // init salesforce functionality (adds values to 2 hidden fields and hides the client/not_client radio buttons
    salesForceInit();

    var $form1 = $("#lead_form");
    var form_1_parsley = $form1.parsley();
    addSubmittingLoader($form1, form_1_parsley);

    var $submit = $("#form_submit");

    $form1.on('submit', function (e) {
        e.preventDefault();

        if (form_1_parsley.isValid()) {

            $submit.prop("disabled", true).addClass('sending');

            //Handle fields and prepare xpm_data object for submission

            //Handle phone number
            var fullphone = $("#form_fullphone").val();
            var areaPhone = xpm_data.custAreaPhone2 = fullphone.slice(0, -7);
            // their phone prefixes are without the first 0
            if (areaPhone.length > 2) {
                areaPhone = areaPhone.substr(1);
            }
            var phoneNumber = xpm_data.custPhone2 = fullphone.slice(-7);
            $('#custAreaPhone').val(areaPhone);
            $('#custPhone').val(phoneNumber);

            // Add other fields to xpim submit
            xpm_data.custName = $("#form_fullname").val();


            // **
            // In case we need different file numbers depending on Client / Not Client
            // **
            // if ($('#form_checkbox_no').is(':checked')) {
            //     $('#fileNo').val("3354");
            //     xpm_data.fileNo = 3354;
            // } else if (!$('#form_checkbox_no').is(':checked')) {
            //     $('#fileNo').val("2644");
            //     xpm_data.fileNo = 2644;
            // }

            if (!env_production) {
                submitToXpm();
                $top_wrapper.addClass("thanks");
                return false;
            }

            if (env_production) {

                if (!$form1.valid()) {
                    alert('אירעה שגיאה בשליחת הנתונים. אנא נסו שוב מאוחר יותר');
                    $submit.prop("disabled", false).removeClass('sending');
                    return false;
                }
            }

            return true
        }
        return false;
    });

});

//initialize the form (Only in Production)
function BIformInit() {
    BI.Lead.init("lead_form");
}

// init salesforce related functionality
function salesForceInit() {
    // only if this param exists
    var mc_sk = getQueryStringParam('mc_sk');
    if(mc_sk) {
        var mc_campaign_code = getQueryStringParam('mc_campaign');
        $('#mc_sk').val(mc_sk);
        $('#mc_campaign_code').val(mc_campaign_code);
        sf_enabled = true;
        // add salesforce values to form
        xpm_data.mc_sk = mc_sk;
        xpm_data.mc_campaign_code =  mc_campaign_code;

        // check the radio client_select YES
        $("#form_checkbox_yes").prop("checked", true);
        // add a class to top_wrapper in case we need to change anything - Hide the Radio buttons
        $("#top_wrapper").addClass('sf-enabled');
    }
}

// function will be called on bezeqint server after the form is submitted
function leadUpdateUi(result) {
    if (result) {
        submitToXpm();
    } else {
        alert('פנייתך לא התקבלה! אנא נסה מאוחר יותר.');
        $("#sub").prop("disabled", false).removeClass('sending');
    }
}

//submit to old leads system then redirect to thank you page
function submitToXpm() {
    window.bbl_leads.send(window.bbl_leads.sanitizeParams(xpm_data)).always(function () {
        if (env_production) {

            var url = [location.protocol, '//', location.host, location.pathname].join('');
            // if p exists then we aren't in index.htm
            // this is for the case that we have for example ?p=gallery and we want to redirect to ?p=gallery-thanks
            var secondaryPage = getQueryStringParam('p');
            var thanksPage = "thanks#thanks";

            // remove this check if you dont want to redirect to different thanks pages
            if (secondaryPage) {
                thanksPage = secondaryPage + "-thanks#thanks"
            }
            window.location.replace(url + "?p=" + thanksPage);
        }
    });
}

// animate anchor scrolls
$('a').on('click', function () {

    // href must start with # but not be only "#"
    if ($(this).attr('href').indexOf("#") === 0 && $(this).attr('href') !== "#") {

        let target_elem = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(target_elem).offset().top
        }, 500);

        return false;
    }
    return true;

});

function addBrowserClasses() {

    if (navigator.userAgent.indexOf('MSIE') > -1) {
        $top_wrapper.addClass('ie');
    }
    if (navigator.userAgent.indexOf('Firefox') > -1) {
        $top_wrapper.addClass('firefox');
    }
    if (navigator.userAgent.indexOf('Edge') > -1) {
        $top_wrapper.addClass('edge');
    }
}

function addIphoneClasses() {

    if (navigator.userAgent.indexOf('iPhone') > -1) {
        $top_wrapper.addClass('iphone');

        if (screen.height <= 480) {
            $top_wrapper.addClass('iphone iphone4');
        } else if (screen.height > 480 && screen.height < 667) {
            $top_wrapper.addClass('iphone iphone5');
        } else {
            $top_wrapper.addClass('iphone iphone6');
        }
    }
}

function onlyNumbers(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}


function getQueryStringParam(param) {
    var queryPairs = window.location.search.slice(1).split("&");

    for (var i in queryPairs) {
        var pair = queryPairs[i].split("=");

        if (pair[0].toLowerCase() === param.toLowerCase()) {
            return pair[1].toLowerCase();
        }
    }

    return '';
}

function detectMobile() {
    return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
}

function fixPlaceholders() {
    // called if firefox or edge browser detected
    $(".form-input.ltr").each(function () {
        var placeholder = $(this).attr('placeholder');
        placeholder = placeholder.slice(0, -1);
        $(this).attr('placeholder', ':' + placeholder);
    });
}

function addSubmittingLoader($form, parsleyInstance) {

    parsleyInstance.on('form:validated', function (parsleyForm) {
        if (true === parsleyForm.validationResult) {
            $form.addClass('loading');
        } else {
            $form.removeClass('loading');
        }
    });
}

function c2c_open() {
    var now = new Date();
    var today = now.getDay();
    var hourNow = now.getHours();
    if (today === 6) return false;
    if (today === 5 && (hourNow >= 12 || hourNow < 8)) return false;
    if (hourNow < 8 || hourNow >= 21) return false;

    return true;
}

function parsleyCustomValidations() {

    var nameRegex = /^[a-zא-תء-يа-я][a-zא-תء-يа-я']*(?:[\-\s]+[a-zא-תء-يа-я][a-zא-תء-يа-я']*)*$/i;

    var fullNameRegex = /^[a-zא-תء-يа-я][a-zא-תء-يа-я']*(?:[\-\s]+[a-zא-תء-يа-я][a-zא-תء-يа-я']*)+$/i;

    Parsley
        .addValidator('firstname', {
            requirementType: 'string',
            validateString: function (value) {

                return nameRegex.test(value)
            },
            messages: {
                en: 'This value should be a name',
                he: 'שם פרטי לא תקין'
            }
        })
        .addValidator('lastname', {
            requirementType: 'string',
            validateString: function (value) {

                return nameRegex.test(value)
            },
            messages: {
                en: 'This value should be a name',
                he: 'שם משפחה לא תקין'
            }
        })
        .addValidator('fullname', {
            requirementType: 'string',
            validateString: function (value) {

                return fullNameRegex.test(value)
            },
            messages: {
                en: 'This value should be a name',
                he: 'שם מלא לא תקין'
            }
        })
        .addValidator('phone', {
            requirementType: 'string',
            validateString: function (value) {

                let regex = /^0(5[012345789]|7[234678]|[23489])[2-9]\d{6}$/;
                return regex.test(value)
            },
            messages: {
                en: 'Please enter a valid phone number',
                he: 'מספר טלפון לא תקין'
            }
        });
}

function localizeParsley() {
    Parsley.addMessages('he', {
        defaultMessage: "לא תקין",
        type: {
            email: "אימייל לא תקין",
            url: "ערך זה צריך להיות URL תקף",
            number: "ערך זה צריך להיות מספר",
            integer: "ערך זה צריך להיות מספר שלם",
            digits: "ערך זה צריך להיות ספרתי",
            alphanum: "ערך זה צריך להיות אלפאנומרי"
        },
        notblank: "שדה חובה",
        required: "שדה חובה",
        pattern: "נראה כי ערך זה אינו תקף",
        min: "ערך זה צריך להיות לכל הפחות %s",
        max: "ערך זה צריך להיות לכל היותר %s",
        range: "ערך זה צריך להיות בין %s ל-%s",
        minlength: "ערך זה צריך להיות לכל הפחות %s תווים",
        maxlength: "ערך זה צריך להיות לכל היותר %s תווים",
        length: "האורך צריך להיות בין %s ל-%s תווים",
        mincheck: "אנא בחר לפחות %s אפשרויות",
        maxcheck: "אנא בחר לכל היותר %s אפשרויות",
        check: "אנא בחר בין %s ל-%s אפשרויות",
        equalto: "ערך זה צריך להיות זהה"
    });

    Parsley.setLocale('he');
}

