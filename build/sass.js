const gulp = require('gulp'),
      sass = require('gulp-sass'),
      concat = require('gulp-concat'),
      util = require(  'gulp-util'),
      autoprefixer=require('gulp-autoprefixer'),
      gulp_minify_css=require('gulp-minify-css');


const path='src/sass/**/*.scss';

module.exports = (gulp) => {

    gulp.task('sass', () => {
       return gulp.src(path)
           .pipe(sass({
               sourceMap: true
           }).on('error', sass.logError))
           .pipe(autoprefixer())
           .pipe(gulp_minify_css())
           .pipe(gulp.dest('./dist/css'))
    });

    gulp.task('sass:watch', () => {
       return gulp.watch(path, ['sass']);
    });
}


