const gulp = require('gulp');
const replace = require('gulp-replace');
const html_replace = require('gulp-html-replace');
const rename = require('gulp-rename');
const htmlmin = require('gulp-htmlmin');

const path = './index.html';

function randomString(length) {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let s = '';
    while (s.length < length) s += chars[Math.floor(Math.random() * (chars.length - 1))];
    return s
}

module.exports = (gulp) => {

    gulp.task('prodhtml', function () {
        return gulp.src(path)
            .pipe(replace('dist/', config.server_path))
            .pipe(replace('src/', config.server_path))
            .pipe(replace('CLIENT_VERSION', randomString(10)))
            .pipe(html_replace({
                devcss: ''
            }))
            .pipe(htmlmin({
                collapseWhitespace: false,
                minifyJS: true,
                minifyCSS: true,
                removeComments: true
            }))
            .pipe(gulp.dest('./dist'))
    });

    gulp.task('prodthx', function () {
        return gulp.src(path)
            .pipe(replace('dist/', config.server_path))
            .pipe(replace('src/', config.server_path))
            .pipe(replace('CLIENT_VERSION', randomString(10)))
            .pipe(html_replace({
                devcss: ''
            }))
            .pipe(replace('id="top_wrapper"', 'id="top_wrapper" class="thanks"'))
            .pipe(rename(function (path) {
                if (path.basename === "index") {
                    path.basename = "thanks";
                } else {
                    if (config.thanksEach) {
                        path.basename += "-thanks";
                    }
                }
            }))
            .pipe(htmlmin({
                collapseWhitespace: false,
                minifyJS: true,
                minifyCSS: true,
                removeComments: true
            }))
            .pipe(gulp.dest('./dist'))
    });

    gulp.task('prodhtml:watch', () => {
        return gulp.watch(path, ['prodhtml', 'prodthx']);
    });

};
