const gulp = require('gulp');

const paths = {
        'js' : [
            "src/js/lib/*.js"
        ],
        'css' : [
            "src/css/lib/*.css", '!src/css/lib/main.css'
        ],
        'fonts' : [
            'fonts/**'
        ]
    };


module.exports = (gulp) => {

    gulp.task('libs:js',function(){
        return gulp.src(paths.js)
            .pipe(gulp.dest('dist/js/lib/'));
    });

    gulp.task('libs:css',function(){
        return gulp.src(paths.css)
            .pipe(gulp.dest('dist/css/'));
    });


    gulp.task('libs:fonts',function(){
        return gulp.src('src/fonts/**')
            .pipe(gulp.dest('dist/fonts/'));
    });

};